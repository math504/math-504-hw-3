clear,clc,close all
%% INIT
load('illposed.mat')
x = linspace(-0.5,0.5,101);
f = sin(10*pi*x);
var = 0;
noise = var^2*randn(size(A,1),1);
F = A*f'+noise;
tol = 1E-15;
%% SVD - LEAST SQUARES
f_svd = (pinv(A,tol)*F)';
%% RELATIVE ERROR
lsq_error = sum((f-f_svd).^2)
%% PLOT
plot(x,f,'k')
hold on
plot(x,f_svd,'r')
grid on
xlim([-0.5,0.5])
ylim([-1.5,1.5])
xlabel('x')
ylabel('f(x)')
title(['\sigma = ',num2str(var)])
legend('f','f_{SVD}','location','northeast')
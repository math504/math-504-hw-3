function [Sol] = factorize4( A, num, y, x )
n= length(A);
Sol = zeros( n,n,4 );

A1 = zeros( length(A) ); A2 = zeros( length(A) );
A3 = zeros( length(A) ); A4 = zeros( length(A) );

if x == 1 && y == 1
    N = num/2;
    A1( 1:N, 1:N) = A( 1:N, 1:N ); 
    A2( 1:N, N+1:2*N ) = A( 1:N, N+1:2*N );
    A3( N+1:2*N, 1:N ) = A( N+1:2*N, 1:N ); 
    A4( N+1:2*N, N+1:2*N ) = A( N+1:2*N, N+1:2*N );
else
    N = num/2;
    if x == 1
        X = 1; Y = (y-1)*num;
        A1( Y+1:Y+N, X:X+N-1) = A( Y+1:Y+N, X:X+N-1);
        A2( Y+1:Y+N, X+N:X+2*N-1 ) = A( Y+1:Y+N, X+N:X+2*N-1 );
        A3( Y+N+1:Y+2*N, X:X+N-1 ) = A( Y+N+1:Y+2*N, X:X+N-1 );
        A4( Y+N+1:Y+2*N, X+N:X+2*N-1 ) = A( Y+N+1:Y+2*N, X+N:X+2*N-1 );
    elseif y == 1
        Y = 1;
        X = (x-1)*num;
        A1( Y:N, X+1:X+N) = A( Y:N, X+1:X+N);
        A2( Y:N, X+N+1:X+2*N ) = A( Y:N, X+N+1:X+2*N );
        A3( Y+N:Y+2*N-1, X+1:X+N ) = A( Y+N:Y+2*N-1, X+1:X+N );
        A4( Y+N:Y+2*N-1, X+N+1:X+2*N ) = A( Y+N:Y+2*N-1, X+N+1:X+2*N );
    else
        X = (x-1)*num; Y = (y-1)*num;
        A1( Y+1:Y+N, X+1:X+N) = A( Y+1:Y+N, X+1:X+N);
        A2( Y+1:Y+N, X+N+1:X+2*N ) = A( Y+1:Y+N, X+N+1:X+2*N );
        A3( Y+N+1:Y+2*N, X+1:X+N ) = A( Y+N+1:Y+2*N, X+1:X+N );
        A4( Y+N+1:Y+2*N, X+N+1:X+2*N ) = A( Y+N+1:Y+2*N, X+N+1:X+2*N );
    end
    N = num/2;
end
Sol(:,:,1) = A1; Sol(:,:,2) = A2; Sol(:,:,3) = A3; Sol(:,:,4) = A4;
function [ answer ] = admissible( A, p )
I = length(A);
flag1 = false;
for i=1:I
    for j=1:I
        if A(i,j) ~= 0
            y_u = i;
            x_l = j;
            y_d = i+I/p - 1;
            x_r = j+I/p - 1;
            flag1 = true;
            if i >= j
                d = i-1;
                X = x_r;
                Y = y_u;
            else
                d = j-1;
                X = x_l;
                Y = y_d;
            end
            break
        end
    end
    if flag1
        break
    end
end

s1 = X/d;
s2 = Y/d;

if X < d && s1 < 1
    answer = true;
elseif X > d && s2 < 1
    answer = true;
else
    answer = false;
end

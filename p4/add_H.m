function [H] = add_H( A, H )
[ n, m, l] = size( H );
H(:,:,l+1) = A;
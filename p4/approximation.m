function [H2] = approximation( A, k, tol )
N = length( A );
H2 = zeros(N);
for i=1:N
    flag1 = false;
    for j=1:N
        if A(i,j) ~= 0
            x_l = j;
            y_u = i;
            s = 0;
            while s ~= 1
                if A(i,j) == 0
                    x_r = j - 1;
                    y_d = i + (j-x_l) - 1;
                    s = 1;
                elseif j == N
                    x_r = N;
                    y_d = i + (x_r-x_l);
                    s = 1;
                end
                j = j+1;
            end
            flag1 = true;
            break
        end
    end
    if flag1
        break
    end
end

B = zeros( (x_r-x_l), k-1 );
C = zeros( (x_r-x_l), k-1 );
x0 = ((x_l/N) + (x_r/N))/2;

for m=0:k-1
    f = @(x) (x - x0).^m;
    for i=x_l:x_r
        B(i-x_l+1,m+1) = integral(f,i/N,(i+1)/N);
    end
end

for m=0:k-1
    for j=y_u:y_d
        if m == 0
            f = @(y) log( abs( x0 - y ) );
            C(j-y_u+1,m+1) = integral(f,j/N,(j+1)/N);
        else
            f = @(y) (x0 - y).^(-m);
            C(j-y_u+1,m+1) = ((-1)^(m+1))*m^(-1)*integral(f,(j)/N,(j+1)/N);
        end
    end
end

S = B*C';
H2(y_u:y_d,x_l:x_r) = S;

for i=y_u:y_d
    for j=x_l:x_r
        if abs(H2(i,j)) <= tol
            H2(i,j) = 0;
        end
    end
end
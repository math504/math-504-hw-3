function [A,H] = admissible2( S2, A, H, p )
for i=1:4
    [ answer ] = admissible( S2(:,:,i), p );
    if answer
        H = add_H( S2(:,:,i), H );
        A = A - S2(:,:,i);
    end
end
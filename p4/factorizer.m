function [A,H] = factorizer( A, H, n )

N = length(A);

% -------- First Factor ---------------------------------
[S2] = factorize4( A, 256, 1, 1 );
[A,H] = admissible2( S2, A, H, 4 );

[S2] = factorize4( A, 256, 1, 2 );
[A,H] = admissible2( S2, A, H, 4 );

[S2] = factorize4( A, 256, 2, 1 );
[A,H] = admissible2( S2, A, H, 4 );

[S2] = factorize4( A, 256, 2, 2 );
[A,H] = admissible2( S2, A, H, 4 );

% Begin Iterations to factorize for the rest
for j=3:n
    common = 2^j;
    block_size = 2*(N/common);
    
    [S2] = factorize4( A, block_size, 1, 1 );
    [A,H] = admissible2( S2, A, H, common );
    
    [S2] = factorize4( A, block_size, 1, 2 );
    [A,H] = admissible2( S2, A, H, common );
    
    for i=2:(common/2)-1
        [S2] = factorize4( A, block_size, i, i-1 );
        [A,H] = admissible2( S2, A, H, common );

        [S2] = factorize4( A, block_size, i, i );
        [A,H] = admissible2( S2, A, H, common );

        [S2] = factorize4( A, block_size, i, i+1 );
        [A,H] = admissible2( S2, A, H, common );
    end
    
    [S2] = factorize4( A, block_size, common/2, common/2-1 );
    [A,H] = admissible2( S2, A, H, common );

    [S2] = factorize4( A, block_size, common/2, common/2 );
    [A,H] = admissible2( S2, A, H, common );
    
end
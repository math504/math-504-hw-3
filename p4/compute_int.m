function [A] = compute_int( A );
N = length(A);
f = @(x,y) log( abs( x-y) );
for i = 1:N
    for j = 1:N
        if A(i,j) ~= 0 && i ~= j
            A(i,j) = integral2( f, i/N, (i+1)/N, j/N, (j+1)/N );
        end
    end
end
            
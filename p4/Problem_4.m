clear all
clc

format long

load('hmatrix.mat')

rng(12345);
N = length(A);
h = 1/N;
A = A*h^2;
T = A;

x = rand( N, 1 );
sol1 = A*x;
H = zeros( N, N );

[A,H] = factorizer( A, H, 5 );

% spy( A )

% ------- Approximation --------------------
k = 1;
tol = 1e-7;
[m,n,l] = size(H);

% A = compute_int( A );

for i=2:l
    H(:,:,i) = approximation( H(:,:,i), k, tol );
end

% figure(1)
% for i=2:7
%     spy(H(:,:,i),'g')
%     hold on
% end
% for i=8:25
%     spy(H(:,:,i),'b')
%     hold on
% end
% 
% for i=26:67
%     spy(H(:,:,i),'r')
%     hold on
% end
% S = H(:,:,60);
% spy( S )

for i=2:l
    A = A + H(:,:,i);
end

% spy(A)
S = T-A;
n1 = norm( S, inf )


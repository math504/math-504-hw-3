%% Math 504 Homework 3 Problem 1a
clear all
clc

format long
rng(12345)

N = 5;
D = diag((1:N)');
D = diag([1,-1,1,1,1]');
V = randn(N);   % Interesting results if orthogonal matrix here instead!
A = V*D/V;

tol = 1e-8;

% A = 10*rand(N);
tic
[V1,d1] = eig(A);
%[V1,d1] = cdf2rdf(V1,d1);
t3 = toc;
d1 = sort( diag(d1) );

tic
[e1,it,Q1] = QR(A,tol);

T = Q1'*A*Q1;

t1 = toc;
e1 = sort(e1);

% T = Q1'*A*Q1;

% %% Part b - Hessenburg Reductions and simple shifts
% H = hess(A);
% 
% tic
% [d2,iter] = qralgt(H,tol);
% t2 = toc;
% 
% d2 = sort(d2);
% 
% %% Compare Methods and print results
% fprintf( 'Method       |  Time     |  Smallest EigenValue   | Largest EigenValue \n' ) 
% fprintf( 'Basic QR     | %.3f sec | %.16d | %.16d \n', t1, e1(1), e1(end) )
% fprintf( 'QR w/ Shifts | %.3f sec | %.16d | %.16d \n', t2, d2(1), d2(end) )
% fprintf( 'Matlab eig   | %.3f sec | %.16d | %.16d \n', t3, d1(1), d1(end) )

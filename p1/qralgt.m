function [d,it] = qralgt(A,tol)
% QR algorithm with shifts and deflation.

% Iterate over eigenvalues
iter = zeros(length(A),1);
it = 0;

for n = length(A):-1:2
  % QR iteration
  for i=1:1e6
    s = A(n,n);
    [Q,R] = MGS(A-s*eye(n));
    A = R*Q + s*eye(n);
    iter(n) = iter(n) + 1;
    it = it + 1;
    e = A(n,n);
    if i > 4
        err = abs( ( e - e1 ) ./ e );
        if err <= tol
            break
        end
    end
    e1 = e;
  end 
  % Deflation
  d(n) = A(n,n);
  A = A(1:n-1,1:n-1);
end
% Last remaining eigenvalue
d(1) = A(1,1);
function [e1,it,Q1] = QR(A,tol)

T0 = A;
[Q1,R] = MGS(T0);
Tk = R*Q1;
it = 0;

for i=1:1e7
    [Q,R] = MGS(Tk);
    Tk = R*Q;
    e = diag( Tk );
    Q1 = Q1*Q;
    if i > 4
        err = abs( ( e - e1 ) ./ e );
        if err <= tol
            break
        end
    end
    e1 = e;
end
it = i;
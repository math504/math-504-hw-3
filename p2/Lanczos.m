function Tk = Lanczos(A,k)
b = rand(size(A,1),1);
q = zeros(size(A,1),k+1);
q(:,1) = b/norm(b);
beta = zeros(1,k);
for j=1:k
    z = A*q(:,j);
    alpha = q(:,j)'*z;
    if j == 1
        z = z - alpha*q(:,j);
    else
        z = z - alpha*q(:,j) - beta(1,j-1)*q(:,j-1);
    end
    beta(1,j) = norm(z);
    q(:,j+1) = z/beta(1,j);
end
Qk = q(:,1:k);
Tk = Qk'*A*Qk;
end
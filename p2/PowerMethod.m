function [evec,eval] = PowerMethod(A,x0,k)
evec = x0;
for i=1:k
       y = A*evec;
    evec = y/norm(y);
    eval = evec'*A*evec;
end
end
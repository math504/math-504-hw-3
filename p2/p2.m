clear,clc,close all
%% MATRIX
n = 9;
A = diag(linspace(0,0.9,n));
%% LANCZOS
k = 1:n;
L = cell(1,length(k));
for i = 1:length(k)
Tk = Lanczos(A,k(i));
L{1,i} = eig(Tk);
end
%% POWER METHOD
PM = zeros(1,length(k));
x0 = rand(n,1);
for i = 1:length(k)
[evec,eval] = PowerMethod(A,x0,k(i));
PM(1,i) = eval;
end
%% PLOT
% Lanczos
LM = zeros(1,length(k));
for i=1:length(k)
    temp = L{1,i};
    LM(i) = max(temp);
    for j = 1:length(temp)
        plot(i,temp(j),'k*')
        hold on
    end
end
% true eigenvalues
T = eig(A);
for i=1:length(T)
    plot(n,T(i),'b*')
end
plot(1:length(k),PM,'r*-')
% formatting
xlim([0,length(k)+1])
ylim([0,1])
xlabel('Lanczos step')
ylabel('Eigenvalues')
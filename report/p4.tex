Here we demonstrate that the matrix found in \textbf{hmatrix.mat} (available on the course homepage) can be represented in H-matrix format for a fixed tolerance. The elements in \textbf{hmatrix.mat} are essentially $a_{ij} \approx log( x_j - x_i )$ and correspond to a simple discretization of the integral equation
\begin{equation}
\int_0^1 \log \vert x-y \vert u(y) dy = f(x).
\end{equation}
To represent the matrix \textbf{hmatrix.mat} in H-matrix format, we begin by factoring it into admissible and inadmissible blocks, where the number of basis function to factor by is $n=2^p$.  A block is said to be admissible if it's width is less than or equal to it's distance to the diagonal. Figures \ref{fig:p4a} and \ref{fig:p4b} show the admissible blocks for $p=3,4$.

\begin{figure}[!htb]
  \centering
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{p4a.png}
    \caption{$p=3$}
    \label{fig:p4a}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{p4b.png}
    \caption{$p=4$}
    \label{fig:p4b}
  \end{minipage}
\end{figure}
After the matrix has been factored into admissible and inadmissible blocks, the admissible blocks are approximated using the first $k$ terms of the Taylor series expansion of the kernel around $x_0 = \frac{a+b}{2}$. This expansion accomplishes two important goals: 1) the double integral that arises from the application of Galerkin's method to the integral equation is separated into two separate integrals, one for $x$ and one for $y$. 2) A general admissible sub-block of $\tilde{A} \, \epsilon \, \mathbb{R}^{n \times n}$ say $\tilde{S} \, \epsilon \, \mathbb{R}^{q \times q}$, can be represented in factorized form $\tilde{S} = BC^T$, $B \epsilon \mathbb{R}^{q \times k}$, and $C \epsilon \mathbb{R}^{q \times k}$ where the entries of $B$ and $C$ are 

\begin{align*}
b_{im} &= \int_{\frac{i}{n}}^{\frac{i+1}{n}} \left( x-x_0 \right)^m dx, \; 0<m<k-1 \\
c_{jm} &= \left( -1 \right)^{m-1} m^{-1} \int_{\frac{j}{n}}^{\frac{j+1}{n}} \left( x_0 - y \right)^{-m} dy, \; 1<m<k-1 \\
c_{jm} &= \int_{\frac{j}{n}}^{\frac{j+1}{n}} \log \vert x_0 - y \vert dy, \; m=0.
\end{align*}

Using this approximation for the kernel in the admissible blocks, the theoretical global error should follow 
\begin{equation} \label{eqn:4a}
\vert \vert A - \tilde{A}_k \vert \vert_{\infty} \leq \frac{3}{2} 3^{-k} n h^2,
\end{equation}
where $k$ is the rank of the sub matrix, $n$ is the size of the overall matrix, and $h$ is the spacing between points. From Equation \ref{eqn:4a}, as $k$ increases the global error should decrease for a fixed tolerance. However, it was found that as $k$ increased this error increased in our numerical experimentation, refer to Table \ref{tab:4a}.

\begin{table}[!htbp]
\centering
\begin{tabular}{|l|l|l|l|l|l|}
\hline
\multicolumn{2}{|l|}{p = 3, tol = 1e-7} & \multicolumn{2}{l|}{p = 4, tol = 1e-7} & \multicolumn{2}{l|}{p = 5, tol = 1e-7} \\ \hline
k                 & err                 & k                 & err                & k                 & err                \\ \hline
1                 & 0.0012657    & 1     & 0.001980  & 1   & 0.002464 \\ \hline
5                 & 0.0014678   & 5     & 0.002211  & 5   & 0.002709 \\ \hline
10               & 0.0014680   & 10   & 0.002211  & 10  & 0.002709 \\ \hline
\end{tabular}
\caption{Comparison of error and $k$}
\label{tab:4a}
\end{table}

It is unclear as to why there is a discrepancy between the theoretical prediction for the error and the numerical experimentation, our best guess is that there is an unresolved error in the code for approximating the kernel in the sub-blocks. To our best knowledge the exact methodology of the problem was followed, however numerical results could not be made to match the theory.

Hierarchical matrices are used to 1) approximate non-sparse matrices as sparse matrices and to 2) speed up matrix vector multiplication. By approximating a non-sparse matrix as sparse the total amount of storage can be reduced from $\mathcal{O}(n^2)$ to $\mathcal{O}(n)$ and the number of operation for a matrix-vector operation is reduced by a similar amount. Additionally, by using a $k$ rank approximation of some matrix $\tilde{A} \, \epsilon \, \mathbb{R}^{n \times n}$ with $k << n$, the matrix-vector multiplication $\tilde{A}x = \sum_{j=1}^k u_jv_j^Tx$ is reduced from $\mathcal{O}(n^2)$ to $\mathcal{O}(kn)$. This is why the H-matrix format can be used to speed up the solution of this equation.
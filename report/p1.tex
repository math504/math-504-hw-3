\subsection*{Part a}
A basic vanilla QR iteration without shifts or an initial reduction to Hessenberg form was implemented and tested on a variety of simple $N \times N$ matrices. The test matrices were created by generating a diagonal matrix of predetermined entries, and then multiplying and diving it by a random matrix. By generating matrices using this procedure, random matrices were created with known eigenvalues (diagonal entries of the diagonal matrix). Two  specific cases where examined 1) when QR iteration converges to the Schur form, and 2) when it does not, we examine the latter first.

Take the following $4 \times 4$ matrix $A$, generated with a diagonal matrix with entries $(1,1,-1,1)$, after $1 \times 10^{8}$ iterations the following orthogonal $Q$ matrix is generated (note $Q=Q_1 Q_2 ... Q_n$),
$$
Q =
\begin{pmatrix}
0.5209 & -0.6436 &	0.4595 & 0.3215 \\
0.1778 & 0.7243 &  0.5457 & 0.3819 \\
0.7883 & 	0.2335 & -0.1917 & 	-0.5360 \\
0.2749 & 0.0814 & -0.6741 & 0.6808
\end{pmatrix}
.
$$
Using matrix $Q$ to write matrix $A$ in Schur form gives,
$$
T = Q'AQ =
\begin{pmatrix}
0.05125 & -0.2809 & 2.3267 & 	27.62 \\
-0.06335 & 0.9812 & 0.1553 &	1.844 \\
0.04523 & 0.01339 & 0.8890 & -1.3167 \\
0.03165 & 0.009373 & -0.0776 & 	0.07841
\end{pmatrix}
.
$$
Clearly matrix $T$ is not an upper triangular matrix, and therefore the QR iteration has not converged to Schur form. The main reason that the method does not converge is due to the fact that not all eigenvalues have distinct absolute values, e.g., ($abs(-1) = abs(1)$).

Next we examine a case where the QR algorithm converges to Schur form. Take any random $5 \times 5$ matrix with known eigenvalues of $(1,2,3,4,5)$, using the basic QR algorithm an orthogonal matrix Q can be generated, and matrix $A$ can be written in the following Schur form,
$$
T = Q'AQ =
\begin{pmatrix}
5.000 & -0.07766 & 2.103 & 2.226 & -1.209 \\
1.671e-06 & 4.000 & 0.1443 & 5.381 &	-90.303 \\
2.333e-14 &	-5.771e-09 & 3.00 &  -0.2840 & -20.94 \\
7.776e-15 &	1.3517e-15 & -1.243e-10 & 2.000	& -9.455 \\
-8.145e-14 & 1.798e-13 & 1.597e-14 &	2.145e-13 &	1.000
\end{pmatrix}
.
$$
It is clear that matrix $T$ is an upper triangular matrix with the eigenvalues as the diagonal, and hence we conclude that the QR algorithm converged to Schur form.

Lastly, for the basic QR algorithm the theoretical convergence rate was compared to the experimental one. The experimental convergence rate was determined by looking at the size of the sub-elements of matrix $A$ as it converges to Schur form. From theory the elements of $A_k$ below the diagonal converge to zero as,
\begin{equation} \label{eqn:convergence}
\mid a_{ij}^k \mid = \mathcal{O} \left( \mid \lambda_i / \lambda_j \mid^k \right), \; i < j.
\end{equation}
For a specific test problem a random matrix was created with known eigenvalues $10,9,8,7,6,5,4,3,2,1$, from Equation \ref{eqn:convergence} the convergence rate of the sub-diagonal element $a_{2,1}$ is $ \mathcal{O} \mid 0.9 \mid^k $. Figure \ref{p1}, shows the comparison of the theory and the numerical results. It is clear from Figure \ref{p1} that the numerical and theoretical convergence rates agree, because the slope of the numerical convergence rate approaches that of the theoretical one.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.65\textwidth]{p1a.png}
	\caption{Theoretical convergence rate compared to the actual convergence rate for the element $a_{2,1}$ in a $10 \times 10$ random matrix with known eignevalues. }
	\label{p1}
\end{figure}

\subsection*{Part b}
Next Hessenberg reduction and simple shifts were added to the simple QR iteration method, and one eigenvalue after the other was computed to high precision on a smaller and smaller matrix. This method was tested on the case 1) matrices, and the speedup with the new method is shown in Table \ref{table:p1a}.

\begin{table}[ht] 
\centering
\begin{tabular}{|l|l|l|l|}
\hline
Method &  Time & Smallest Eigenvalue & Largest Eigenvalue  \\ \hline
Basic QR & 2.962 sec & 1.0000000000000830e+00 & 9.9999998523846244e+01 \\ \hline
QR w/ Shifts & 0.493 sec & 1.0000000000000284e+00 & 9.9999999999998977e+01 \\ \hline
\end{tabular}
\caption{Comparison of comparison of basic QR iteration, and QR iteration with initial reduction to Hessenberg form and simple shifts}
\label{table:p1a}
\end{table}

It is clear from Table \ref{table:p1a}, that by implementing initial reduction to Hessenberg form and by adding in simple shifts the total computational cost for the QR algorithm is greatly reduced.
We implemented the Lanczos method and tested it on several symmetric matrices. To verify the convergence pattern shown in Demmel Fig. 7.2 and the interlacing property of eigenvalues we chose a small symmetric positive matrix consisting of evenly spaced numbers between 0 and 1 on the diagonal. Figure \ref{p2} illustrates the expected convergence pattern and eigenvalue interlacing property.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.65\textwidth]{p2.jpg}
	\caption{Lanczos method for a small symmetric matrix (black stars) with exact solution (blue stars) compared to the power method (red stars).}
	\label{p2}
\end{figure}

The blue stars in the figure are the exact eigenvalues, and the black stars are the approximate eigenvalues at the given Lanczos step. We do indeed see that at the first Lanczos step there is only one approximate eigenvalue. At the second step there are two approximate eigenvalues that lie above and below the previous step. The pattern of increasing the number of eigenvalues and interlacing with the previous step continues for the other Lanczos steps. In fact, if a matrix $A$ is symmetric and has $k$ eigenvalues, then we only need $k$ Lanczos steps to approximate the eigenvalues. Using more than $k$ Lanczos steps would not make any sense, and we would expect to get unusual results if we did such a thing.

From Figure \ref{p2} we see that the Lanczos method converges to the expected solution in at least $k$ steps. The power method was applied to the same problem where the red stars indicate the approximate solution for the largest eigenvalue of the matrix. It is clear that the power method converges to the largest eigenvalue in roughly the same number of steps as the Lanczos method. This, however, is not the case for all possible symmetric matrices. As can be seen in Figures \ref{p2a} and \ref{p2b}, the power method does not always converge to the largest eigenvalue in $k$ iterations. From this we conclude two things about the power method: the method requires a good initial guess, and the matrix must have at least one eigenvalue that is greater in absolute value than the rest. The matrix of Figure \ref{p2a} exemplifies the former conclusion where there is one eigenvalue that is greater in magnitude than the rest, but the power method converges to the second largest eigenvalue due to a poor initial guess. The latter conclusion is demonstrated in the matrix of Figure \ref{p2b} where there is clearly more than one eigenvalue with equal magnitude greater than the rest. In this case the power method converges to some arbitrary eigenvalue of the matrix depending on the initial condition. Hence, for the power method to be of any meaningful use, a good initial guess must be selected for a properly defined matrix in which the eigenvalues are well behaved. 

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.65\textwidth]{p2a.jpg}
	\caption{Power method with good eigenvalues but poor initial guess}
	\label{p2a}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.65\textwidth]{p2b.jpg}
	\caption{Power method with non-unique largest eigenvalue in absolute value}
	\label{p2b}
\end{figure}

Thus, one of the strengths of the Lanczos method over the power method is its ability to approximate all of the eigenvalues and do so in at least $k$ steps. So, for small matrices the Lanczos method works really well. However, when $k$ is very larger we might run into problems when extracting the eigenvalues of a symmetric matrix in a timely manner. Overall, Lanczos is an excellent method for approximating all of the eigenvalues for symmetric matrices.



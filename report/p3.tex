In this problem we considered a highly oscillatory function $f(x)=\sin(10\pi x)$ for $x \in [-0.5, 0.5]$. We let $f \in \mathbb{R}^{101}$ be a vector with entries $f_j = f(x_j)$, which is the function values at a set of 101 equidistant grid points. The matrix $A \in \mathbb{R}^{241\times100}$ is an ill-posed matrix such that $Af=\tilde{f}$ represents a local average of $f$ on a finer grid. In fact, $A$ was computed in such a way that $\tilde{f}$ is a smoothed version of $f$ using a Gaussian kernel.

We are interested in reconstructing $f$ from $\tilde{f}$, i.e., we want to compute $f$ given $A$ and $\tilde{f}$. Since a lot of information about the high-frequency content of $f$ is lost in the averaging process, this is an ill-posed problem. Particularly, we consider this to be a rank-deficient least squares problem. To setup and solve this problem, we first generated a highly oscillatory $f$, which we defined above. Then, we constructed $\tilde{f} = Af$ and used Matlab's built-in command \textbf{randn} to add to $\tilde{f}$ a small amount of noise. In this case, our noise was normally distributed with mean zero and variance $\sigma$. Finally, given $A$ and the noisy data $\tilde{f}$ we reconstructed $f$ in the least squares sense using the truncated SVD.

As a base case, we considered $\tilde{f}$ with only smoothing and no noise, i.e., with $\sigma = 0$. The resulting plot of the original function $f$ and the reconstructed version $f_{SVD}$ is presented in Figure \ref{p3a}. We also considered $\tilde{f}$ with smoothing and noise, particularly with $\sigma = 0.1$, see Figure \ref{p3b}. Note that we used the sum of the squares of the residuals, i.e., $\sum_j (f(x_j)-f_{SVD}(x_j))^2$, as a metric to determine which tolerance $\varepsilon_{TOL}$ gave us the best least squares fit to the data.

\begin{figure}[!htb]
	\centering
	\minipage{0.45\textwidth}
	\includegraphics[width=\linewidth]{p3a.jpg}
	\caption{Fit to smoothed data without noise}
	\label{p3a}
	\endminipage
	\minipage{0.45\textwidth}
	\includegraphics[width=\linewidth]{p3b.jpg}
	\caption{Fit to smoothed data with noise}
	\label{p3b}
	\endminipage
\end{figure}

The truncated SVD fitting method works well on the smoothed data with no noise. However, it begins to fail when noise is added to $\tilde{f}$. How well the method works depends on a number of factors. The noise level in the data clearly affects the result. However, the choice of function $f$ and the interval of interest also play a role. For our purposes, we kept our choice of $f$ and our interval of interest the same, but we explored how the result changes with different tolerances $\varepsilon_{TOL}$ and different noise levels, i.e., different values of $\sigma$. Our results are included in Table \ref{t3}. We immediately see that as the variance of the noise increases the error of the fit and the required tolerance increases. This is to be expected because the problem becomes more and more ill-posed as the noise in the data becomes worse.

\begin{table}[ht]
	\centering
	\begin{tabular}{| c | c | c |}
		\hline
		$\sigma$ & $\varepsilon_{TOL}$ & error \\
		\hline
		0.1   & $10^{-2}$ & 14.0213 \\
		\hline
		0.01  & $10^{-3}$ & 10.8528 \\
		\hline
		0.001 & $10^{-5}$ & 10.4225 \\
		\hline
	\end{tabular}
	\begin{tabular}{| c | c | c |}
		\hline
		$\sigma$ & $\varepsilon_{TOL}$ & error \\
		\hline
		0.0001  & $10^{-8}$  & 8.8737 \\
		\hline
		0.00001 & $10^{-10}$ & 5.8026 \\
		\hline
		0       & $10^{-15}$ & 0.3683 \\
		\hline
	\end{tabular}
	\caption{Least squares results using truncated SVD for different tolerances and noise levels}
	\label{t3}
\end{table}